<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();

		$this->load->model("Login_Model");
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function index(){
		$this->load->view('login');
		if ($this->form_validation->run() === true) {
			$mail = $this->input->post('email');  
			$this->load->view('inscription');
		}
	}

	public function check() {
		$data = array(
			'current'  => $this->input->post("pseudo")
		);
		$this->session->set_userdata($data);
		$pass = $this->input->post('pass');
		$this->load->model('Login_Model','',TRUE); // a regarder
		$cu = $this->Login_Model->checkUser($data,$pass);
		if(!$cu) {
			redirect(base_url());
		} else {
			$data ['id'] = $cu;
			$this->session->set_userdata($data);
			redirect(base_url("index.php/Tchat_Controller/index"));
		}
	}

	public function inscription (){
		$this->form_validation->set_rules("pseudo","Pseudo","trim|required");
		$this->form_validation->set_rules("mail","Mail","trim|valid_email|required");
		$this->form_validation->set_rules("pass","Pass","trim|required");
		$this->form_validation->set_rules("passconfirm","Passconfirm","trim|required|matches[pass]");

		if($this->form_validation->run() === false){
			$this->load->view('inscription');
		}
		else{
			$pseudo = $this->input->post('pseudo');
			$mail = $this->input->post('mail');  
			$pass = $this->input->post('pass');
			$passconfirm = $this->input->post('passconfirm');

			if($this->Login_Model->register($pseudo,$mail,$pass)){
				$this->load->view('tchat');
			}
		}
	}
}