<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tchat_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();

		$this->load->model("Tchat_Model");
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->library('form_validation');
		

		$this->output->enable_profiler(true);
	}

	public function index() {
		$user = $this->session->userdata('current');
		// // $date = $this->session->userdata('dateDemande'); Session de date
		$channel = "General";
		array('data' => $this->Tchat_Model->getMsg($channel));
		$this->load->view('tchat');
	}

	// public function getMessage () {
	// 	$user = $this->session->userdata('current');
	// 	$channel = "General";
	// 	$mydata2 = array('data' => $this->Tchat_Model->getMsg($channel));
	// 	$this->load->view('tchat',$mydata2);
	// }

	// public function getNewMessage () {
	// 	$user = $this->session->userdata('current');
	// 	// $date = $this->session->userdata('dateDemande'); 
	// 	$channel = "General";
	// 	$mydata = array('data' => $this->Tchat_Model->getNewMsg($channel, $date));
	// 	$this->load->view('tchat',$mydata);
	// }

	public function sendMessage (){
			$contenu = $this->input->post('msg');
			$idChannel = 1; // Recupérer l'id  du channel
			$idUtilisateur = $this->session->userdata('id');

			$this->Tchat_Model->sendMsg($contenu,$idChannel,$idUtilisateur);
			header('Location: http://alexm.dijon.codeur.online/projets/TCHAT/index.php/Tchat_Controller/index');
	}
}
