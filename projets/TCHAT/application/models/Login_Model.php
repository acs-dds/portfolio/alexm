<?php 

class Login_Model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	private function hash ($pass) {
		return md5($pass);
	}

	public function register($username,$email,$password){
		$data = array(
			"pseudo"   => $username,
			"email"      => $email,
			"mdp"   => $this->hash($password)
		);
		return $this->db->insert("utilisateur",$data);
	}

	public function checkUser($username,$password) {
		$id = "SELECT id FROM utilisateur WHERE pseudo = ? AND mdp = ?";		
		$query = $this->db->query($id, array($username,md5($password)));
		return $query->row()->id;
	}
}