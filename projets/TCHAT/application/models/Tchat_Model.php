<?php 

class Tchat_Model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	public function sendMsg ($contenu, $idChannel, $idUtilisateur){
		$data = array(
			"contenu"   => $contenu,
			"id_discussion"      => $idChannel,
			"id_utilisateur"   =>  $idUtilisateur
		);
		return $this->db->insert("message",$data);
	}

	public function getMsg($channel) {
		$select = "SELECT utilisateur.pseudo,message.contenu,message.date_message,discussion.intitulé
			FROM message
			JOIN discussion ON message.id_discussion = discussion.id
			JOIN utilisateur ON message.id_utilisateur = utilisateur.id
			WHERE discussion.intitulé = ?";

		$query = $this->db->query($select, array($channel));

		return $query->result_array();
	}

	// public function getNewMsg ($channel, $date) {
	// 	$select = "SELECT utilisateur.pseudo,message.contenu,message.date_message,discussion.intitulé
	// 		FROM message
	// 		JOIN discussion ON message.id_discussion = discussion.id
	// 		JOIN utilisateur ON message.id_utilisateur = utilisateur.id
	// 		WHERE discussion.intitulé = ?
	// 		WHERE message.date_message > ?";

	// 	$query = $this->db->query($select, array($channel, $date));

	// 	return $query->result_array();
	// }

}