<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Tchat | Inscription</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/static/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/static/css/register.css">
	<link href="https://fonts.googleapis.com/css?family=Righteous|Ropa+Sans" rel="stylesheet">

</head>

<body>

	<h2 class="titre">Veuillez vous inscrire !</h2>

<div class="bloc_register">
<?= form_open() ?>
	<div class="formulaire">
		<div class="form-group">
			  <label class="col-md-4 control-label">Pseudo</label>  
			  <div class="col-md-4">
			  <input id="pseudo" type="text" name="pseudo">   
			  </div>
		</div>
		<div class="form-group">
			  <label class="col-md-4 control-label">Adresse mail</label>  
			  <div class="col-md-4">
			  <input id="mail" type="mail" name="mail" value="<?=$_POST['email']?>"> <!-- = echo $_POST['email']  -->    
			  </div>
		</div>
		<div class="form-group">
			  <label class="col-md-4 control-label">Mot de passe</label>  
			  <div class="col-md-4">
			    <input id="pass" type="password" name="pass">   
			  </div>
		</div>
		<div class="form-group">
			  <label class="col-md-4 control-label">Confirmer mot de passe</label>
			  <div class="col-md-4">                     
			    <input id="pass" type="password" name="passconfirm">
			  </div>
		</div>
		<div class="form-group">
			  <div class="col-md-4">                     
		<input class="confirm" id="send" type="submit" value="Confirmer" name="send">
			  </div>
		</div>
	</div>
</form>
</div>
</body>
</html>