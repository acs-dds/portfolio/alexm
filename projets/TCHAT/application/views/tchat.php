<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Tchat</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/static/css/chat.css"> 
    <link href="https://fonts.googleapis.com/css?family=Righteous|Ropa+Sans" rel="stylesheet">


</head>

<body>

  <header>
     <div class="title-header"><h5>Vous êtes sur le salon ACCUEIL<!-- REMPLIR NOM CHANNEL--> !</h5><p class="slogan"><!-- MESSAGE DU CHANNEL --></p></div>
     <div class="btn-status">
        <button style="border-radius: 20px;" type="button" class="btn-stats btn-ligne btn btn-default"></button>
        <button style="border-radius: 20px;" type="button" class="btn-stats btn-absent btn btn-default"></button>
        <button style="border-radius: 20px;" type="button" class="btn-stats btn-horsligne btn btn-default"></button>
      </div>
  </header>


        <!-- ** LES UTILISATEURS SIDE BAR DE DROITE ** -->

          <ul class="sidebar-chat-right nav navbar-nav side-nav">

            <a href="#masquedeco"><button class="log-btn" type="button">➠ Se deconnecter</button></a>

            <h4 style="color:white;margin-top: 16%;margin-left: 10px;">Utilisateurs <small style="color:#7AFF74;">En ligne - 2</small></h4>

              <li style="margin-top:5px;"  class="active">
                  <img class="avatar-bar" src="https://placehold.it/30x30"><a class="user" href="index.html"><!-- NOM USER -->User</a>
              </li>
              <li style="margin-top:5px;"  class="active">
                  <img class="avatar-bar" src="https://placehold.it/30x30"><a class="user" href="index.html"><!-- NOM USER -->User</a>
              </li>
          </ul>

          <!-- ** LES CHANNEL SIDE BAR DE GAUCHE** -->

          <ul class="sidebar-chat nav navbar-nav side-nav">

            <a href="#masque"><button class="channel-create-btn" type="button">✚ Cree un salon</button></a>

              <h4 style="color:white;margin-top: 16%;margin-left: 10px;">Salons Textuels - 3</h4>

              <li style="margin-top:10px;" class="active">
                  <a class="channel" href="index.html"><!-- NOM CHANNEL -->General</a>
                  <a class="channel" href="index.html"><!-- NOM CHANNEL -->Random</a>
                  <a class="channel" href="index.html"><!-- NOM CHANNEL -->Others</a>
              </li>
          </ul>

  ->
            

        

        <?= form_open('Tchat_Controller/sendMessage', 'id="myform"') ?>
        <!--  <form id="myform" action="" method="post"> -->
            <div class="content-chat-msg input-group">
                <input type="text" class="form-control" placeholder="Enter Message" id="msg" name="msg">
                <span class="input-group-btn">
                    <input type="text" name="channel" value="General">
                    <input value="Envoyer" class="btn-send-msg btn btn-info" type="submit" id="btnMsg" > <!-- type button ? -->
                </span>
            </div>
        </form>

          








<!--  ** PARTIE POP UP A NE PAS TOUCHER OU JE T'ENCHAINE ** -->

<style type="text/css">
  
#masque{
  display: none;
  position: fixed;
  background: transparent;
  top:0;bottom: 0;left: 0;right: 0;
  z-index: 1000;
}
#masque:target{
  display: block;
}
#masquedeco{
  display: none;
  position: fixed;
  background: transparent;
  top:0;bottom: 0;left: 0;right: 0;
  z-index: 1000;
}
#masquedeco:target{
  display: block;
}
.fenetre-modale{
  background: rgba(0, 0, 0, 0.94);
  padding:20px;
  position: relative;
  margin: 10% auto;
  width: 50%;
  color: white;

}
.img-bombe{
  float: right;
  margin: 0 0 0 20px;

}
img.btn-fermer{
  float: right;
  margin: -55px -55px 0 0;
}

</style>


<!--   ** POP UP CREE CHANNEL ** -->

<div id="masque">
  <div class="fenetre-modale">

    <a class="fermer" href="#nullepart"><img style="margin-top: -45px;margin-right: -45px;" alt="Fermer la fenêtre" title="Fermer la fenêtre" class="btn-fermer" 
    src="http://img4.hostingpics.net/pics/495740rond2.png" /></a>


    <h2>Pop Up cree channel</h2>
    <p>A modifier .. </p>
    <p>Par alex ... </p>

  </div> 
</div> 

<!-- ** FIN POP UP ** -->


<!--   ** POP UP DECONNEXION ** -->

<div id="masquedeco">
  <div class="fenetre-modale">

    <a class="fermer" href="#nullepart"><img style="margin-top: -45px;margin-right: -45px;" alt="Fermer la fenêtre" title="Fermer la fenêtre" class="btn-fermer" 
    src="http://img4.hostingpics.net/pics/495740rond2.png" /></a>


    <h2>Pop Up deconnection</h2>
    <p>A modifier .. </p>
    <p>Et surtout ne pas toucher ... </p>

  </div> 
</div> 

<!-- ** FIN POP UP ** -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
     /*afficheConversation();*/  
     $("#btnMsg").click(function(e){ // submit ?
      
        function afficheConversation(){
                $.ajax({
                    url : 'http://alexm.dijon.codeur.online/projets/TCHAT/index.php/Tchat_Controller/getMessage',
                    type : 'POST', // Le type de la requête HTTP, ici devenu POST
                    data : $("#msg").serialize(), // "login=farid&discussion=general"
                    success : function(data2){
                    $("#contenu").html(data2);
                    } //fin de success
                });//fin de $.ajax
        }//fin de affiche conversation
        afficheConversation();
        setInterval(afficheConversation, 3000); //on lance la fct afficheConversation toutes les 200ms 
        e.preventDefault();//annule l'action qui va vers une autre page   
    });//$("#btnMsg").click(function(e)
  });//fin de $(document).ready

</script> -->














<!-- <script type="text/javascript">
  
  $(document).ready(function() {
    function afficheMessage() {
        $.ajax({
            url : 'http://alexm.dijon.codeur.online/projets/TCHAT/index.php/Tchat_Controller/getMessage',
            type : 'POST',
            data : msg:$('#msg').val(),
            success : function(data){
              $('#contenu').html(data);
            };// fin success
        }); // fin ajax 
    }; // fin afficheMessage  
    setInterval(afficheMessage, 1000);
  }); // fin ready
</script> -->







        <!-- $("#myform").submit(function afficheMessage() {
        $.ajax({
            url : 'http://alexm.dijon.codeur.online/projets/TCHAT/index.php/Tchat_Controller/getMessage',
            type : 'POST',
            data : $('#myform').serialize(),
            success : function(data){
              $('#contenu').html(data);
            };// fin success
        }); // fin ajax // fin afficherMessage     
    }); // fin click -->


  
</body>
</html>