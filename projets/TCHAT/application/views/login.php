<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Tchat | Login</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/static/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/static/css/login.css">
	<link href="https://fonts.googleapis.com/css?family=Righteous|Ropa+Sans" rel="stylesheet">

</head>

<body>

<div class="login_tchat">

  <section class="bloc_register">
    <h1 class="titre">Tu es nouveau ?</h1>
    <?= form_open('Login_Controller/inscription') ?>
  	<div class="formulaire">
  		<div class="form-group">
  			  <label class="col-md-4 control-label">Votre adresse mail :</label>
  			  <div class="col-md-4">
  			  <input id="email" type="email" name="email">
  			  </div>
  		</div>
  		<div class="form-group">
  			  <div class="col-md-4">
  		<input class="confirm" id="send" type="submit" value="S'inscrire" name="send">
  			  </div>
  		</div>
  	</div>
</form>



  </section>

  <section class="bloc_login">
    <h1>Bon retour parmis nous !</h1>

    <?= form_open("Login_Controller/check") ?>
      	<div class="formulaire">
      		<div class="form-group">
      			  <label class="col-md-4 control-label">Pseudo</label>
      			  <div class="col-md-4">
      			  <input id="pseudo" type="text" name="pseudo">
      			  </div>
      		</div>
      		<div class="form-group">
      			  <label class="col-md-4 control-label">Mot de passe</label>
      			  <div class="col-md-4">
      			    <input id="pass" type="password" name="pass">
      			  </div>
      		</div>
      		<div class="form-group">
      			  <div class="col-md-4">
      		<input class="confirm" id="send" type="submit" value="Se Connecter" name="send">
      			  </div>
      		</div>
      	</div>
    </form>

    <h2>Je veut y acceder en tant que visiteur :</h2>

    <a href="#"><button class="btn-guest">Je suis un visiteur</button></a>

  </section>

</div>

</body>
</html>
