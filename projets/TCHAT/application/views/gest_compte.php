<!DOCTYPE html>
<html>
<head>

  <title></title>
  <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="static/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="static/css/gest_compte.css">
  <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet">
  <style class="cp-pen-styles">#particles-js{ position:absolute; width: 100%;}</style>
  <script src="./index_files/particles.min.js.téléchargement"></script>
  <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

</head>

<body>

  <div style="height:135%;" id="particles-js"></div>
  <script>particlesJS("particles-js", {"particles":{"number":{"value":420,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},
  "polygon":{"nb_sides":4},"image":{"src":"","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":1,"opacity_min":0,"sync":false}},"size":{"value":3,"random":true,
  "anim":{"enable":false,"speed":9.744926547616142,"size_min":0.8120772123013452,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},
  "move":{"enable":true,"speed":1,"direction":"none","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":
  {"detect_on":"canvas","events":{"onhover":{"enable":true,"mode":"bubble"},"onclick":{"enable":false,"mode":"push"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":
  {"distance":250,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true})
  ;var count_particles, stats, update; stats = new Stats; stats.setMode(0); stats.domElement.style.position = 'absolute'; stats.domElement.style.left = '0px'; stats.domElement.style.top = '0px';
  document.body.appendChild(stats.domElement); count_particles = document.querySelector('.js-count-particles'); update = function() { stats.begin(); stats.end()
    ; if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) { count_particles.innerText = window.pJSDom[0].pJS.particles.array.length; } requestAnimationFrame(update); }
    ; requestAnimationFrame(update);
    </script>

  <div class="container">
      <div class="row">

        <div style="margin-left:10%;" class="col-md-9 clearfix" id="customer-account">

          <img style="margin-bottom: 60px;margin-top: 50px;" class="avatar" src="http://placehold.it/140x140">
            <h2 style="text-transform:uppercase;margin-bottom:80px;float:right;margin-right:55%;margin-top:12%;"><!-- PSEUDO DE LA PERSONNE -->Lorenzo</h2>

          <div class="box no-mb">
            <div class="heading">
              <h3 class="text-uppercase">Détails personnels</h3>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Pseudo actuel</label>
                  <input type="text" class="form-control" value="PSEUDO PERSONNE" disabled="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Email actuel</label>
                  <input type="text" class="form-control" value="EMAIL DE LA PERSONNE" disabled="">
                </div>
              </div>
            </div>
          </div>

        <?= form_open() ?>

          <div class="box clearfix no-mb">
            <div class="heading">
              <h3 class="text-uppercase">Changer de mot de passe</h3>
            </div>
            <form method="post" data-ajax="true" action="/user/change_pw">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Nouveau Mot de passe</label>
                    <input type="password" class="form-control" name="password">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Confirmation du mot de passe</label>
                    <input type="password" class="form-control" name="password_confirmation">
                  </div>
                </div>
              </div>
              <div class="text-center">
                <button type="submit" class="btn-send btn btn-template-main"><i class="fa fa-save"></i> Confirmer</button>
              </div>
            </form>
          </div>

        </form>

          <?= form_open() ?>

            <div class="box clearfix no-mb">
              <div class="heading">
                <h3 class="text-uppercase">Changer d'email</h3>
              </div>
              <form method="post" data-ajax="true" action="/user/change_email">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Nouvel Email</label>
                      <input type="email" class="form-control" name="email">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Confirmation de l'email</label>
                      <input type="email" class="form-control" name="email_confirmation">
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn-send btn btn-template-main"><i class="fa fa-save"></i> Confirmer</button>
                </div>
                <div class="box clearfix no-mb">

            </form>

              <?= form_open() ?>

          <div class="heading">
            <h3 class="text-uppercase">Changer d'avatar</h3>
          </div>
          <form method="post" data-ajax="true" action="/user/change_email">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Avatar actuel</label>
                  <img style="margin-left:8%;" src="http://placehold.it/100x100">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Choisir un nouvel avatar</label>
                  <input style="margin-top:3%" type="file" class="form-control" name="email_confirmation">
                </div>
              </div>
            </div>
            <div class="text-center">
              <button style="margin-top:-4%;margin-bottom:50px;" type="submit" class="btn-send btn btn-template-main"><i class="fa fa-save"></i> Confirmer</button>
            </div>
                <br>
            </div>
        </div>
    </div>
</form>
