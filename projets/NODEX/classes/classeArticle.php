<?php 
	class Article {
		
		private $id;
		private $matiere;
		private $longueur;
		private $largeur;
		private $epaisseurs;
		private $prixGache;
		private $prixDecoupe;
		private $image;

		public function __construct($array)
		{
			$this->id = $array[0];
			$this->matiere = $array[1];
			$this->longueur = $array[2];
			$this->largeur = $array[3];
			$this->epaisseurs = array_map('trim', explode(',', $array[4]));
			$this->prixGache = $array[6];
			$this->prixDecoupe = $array[7];
			$this->image = $array[8];



		}

		public function getMatiere () {
			return $this->matiere;
		}

		public function getLongueur () {
			return $this->longueur;
		}

		public function getLargeur () {
			return $this->largeur;
		}

		public function getEpaisseurs () {
			return $this->epaisseurs;
		}

		public function getId () {
			return $this->id;
		}

		public function getPrixGache () {
			return $this->prixGache;
		}

		public function getPrixDecoupe () {
			return $this->prixDecoupe;
		}

		public function getImage () {
			return $this->image;
		}
	}
?>