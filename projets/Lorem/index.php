<!DOCTYPE html>
<html>
<head>

	<title>Genérateur de Texte</title>
	<link href="https://fonts.googleapis.com/css?family=Playball" rel="stylesheet">
</head>

<body>

<style type="text/css">
	
body{
	background-image: url('http://alexm.dijon.codeur.online/projets/Lorem/fond.png');
}
h1{
	color: white;
	text-align: center;
	border-bottom: 1px solid white;
}
#nb{
	width: 3%;
	padding: 1%;
	background-color: transparent;
	border: 1px solid white;
	margin-top: -24%;
	color: white;
	outline: none;
}
.tms{
	margin-top: 3%;
	margin-left: 44%;
	color: white;
}
#gen{
	padding: 1%;
	background-color: transparent;
	border: 1px solid white;
	color: white;
	width: 5%;
	margin-left: 45%;
	margin-top: 3%;
	outline: none;
}
#gen:hover{
	background-color: white;
	border: none;
	color: black;
}
#gen:active{
	background-color: grey;
}
.rendu{
	color: white;
	width: 80%;
	border: 1px solid white;
	text-align: center;
	margin-top: 5%;
	font-size: 22px;
	font-family: 'Playball', cursive;
}

</style>

<h1>Générateur de Texte</h1>

<form>

	<input id="gen" type="submit" value="Générer"/>
	<input type="number" name="nb" id="nb" value="0"/><br/>
<div class="tms">
	<input id="thm" type="checkbox" name="themes[]" value="game"/> Gaming
	<input id="thm" type="checkbox" name="themes[]" value="space"/> Spacial
	<input id="thm" type="checkbox" name="themes[]" value="food"/> FastFood
</div>
</form>

<center><div class="rendu">
<?php
	require_once __DIR__.'/classes/controller.php';

	$c = new Controller();

	if (isset($_GET['nb'])): ?>
<?php
		if (isset($_GET['themes'])):
			echo $c->genererParagraphesAction($_GET['nb'], $_GET['themes']);
		else:
			echo $c->genererParagraphesAction($_GET['nb']);
		endif;
	else: ?>
<?php
	echo $c->genererTexteAction();
	endif;
?>
</div></center>

</body>
</html>
