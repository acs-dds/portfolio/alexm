<?php

class Mapper {
	private $tab;

	private $themes = [
		'game' => ['Gaming','Fps','kill','sniper','shoot','Headshot','tir'],
		'space' => ['Espace','etoiles','fusee','astronaute','spacial','planetes'],
		'food' => ['burger','frites','sodas','service','glaces','manger','rapide','sandwich','desserts']
	];

	public function __construct() {
		$this->tab = ["maecenas", "sit", "amet", "aliquam", "nunc", "ac", "pellentesque", "nisl", "nunc", "sapien", "ligula", "pellentesque", "quis", "cursus", "eget", "finibus", "et", "quam", "morbi", "ornare", "ullamcorper", "convallis", "ut", "at", "leo", "varius", "pretium", "felis", "eget", "tempus", "arcu", "aenean", "aliquet", "lorem", "quis", "auctor", "egestas", "nam", "vitae", "ipsum", "a", "lectus", "pretium", "venenatis", "pulvinar", "eu", "est", "morbi", "ullamcorper", "nibh", "sit", "amet", "enim", "sagittis", "at", "volutpat", "ligula", "placerat", "duis", "sem", "augue", "tempor", "sed", "massa", "cursus", "aliquam", "blandit", "nisi", "sed", "eleifend", "tincidunt", "dui", "quis", "laoreet", "massa", "efficitur", "et", "sed", "dui", "sapien", "volutpat", "et", "finibus", "nec", "ullamcorper", "suscipit", "mauris", "cras", "ultrices", "ex", "et", "nulla", "congue", "tempus", "curabitur", "finibus", "commodo", "tristique", "nullam", "pharetra", "ex", "at", "purus", "sodales", "non", "faucibus", "neque", "scelerisque"];
	}

	public function chargerTheme($nom) {
		if (isset($this->themes[$nom])) {
			$this->tab = array_merge($this->tab, $this->themes[$nom]);
		} else {
			echo 'Thème '.$nom.' inexistant';
		}
	}

	public function genererTexte($nombre) {
		$ret = [];

		for ($i=0; $i < $nombre; $i++) { 
			$ret[] = $this->genererParagraphe();
		}

		$ret = implode(PHP_EOL.PHP_EOL, $ret);

		return $ret;
	}

	public function genererParagraphe() {
		

		$longueurBase = count($this->tab);

		$longueurParagraphe = rand(50, 150);

		$ret = [];
		for ($i=0; $i < 100; $i++) { 
			$indice = rand(0, $longueurBase - 1);
			$ret[] = $this->tab[$indice];
		}

		$ret = implode(" ", $ret);

		return "<p>$ret</p>";
	}
}