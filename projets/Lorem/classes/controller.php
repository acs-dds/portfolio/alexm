<?php

require_once __DIR__.'/mapper.php';

class Controller {
	private $mapper;

	public function __construct() {
		$this->mapper = new Mapper();
	}

	public function genererTexteAction() {
		return $this->mapper->genererParagraphe();
	}

	public function genererParagraphesAction($nb, $themes = []) {
		foreach ($themes as $nomDeTheme) {
			$this->mapper->chargerTheme($nomDeTheme);
		}
		return $this->mapper->genererTexte($nb);
	}
}