<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilisateur extends CI_Controller {

	public function profil($id){

		$data = array('nom' => 'Alex');

		$page = $this->load->view('utilisateur/profil', $data, true);

		$this->load->view('template', array('page' => $page));
	}

}
