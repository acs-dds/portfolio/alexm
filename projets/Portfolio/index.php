<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Portfolio | Alex</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Thambi|Pacifico" rel="stylesheet">

</head>

<body>

<!-- *** Bannière acceuil *** -->

 <h2 class="titre_accueil">Portfolio</h2>
 	<p class="texte_accueil">Voici le portfolio !</p>
 		<div class="slideshow">
 			<ul>
				<li><img src="img/banniere_accueil.jpg"></li>
				<li><img src="img/banniere_accueil2.jpg"></li>
				<li><img src="img/banniere_accueil3.jpg"></li>
				<li><img src="img/banniere_accueil4.jpg"></li>
			</ul>
	 	</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script type="text/javascript" src="js/slide.js"></script>

<!-- *** Barre de naviguation *** -->
<?php require "../../nabilb/menu.php"; ?>
<!-- 		<nav class="menu navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header" id="exCollapsingNavbar2">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynav">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="mynav">
					<ul class="nav navbar-nav">
						<li><a href="index.html">Presentation</a></li>
						<li><a href="projets.html">Projets</a></li>
						<li><a href="cv.html">Curriculum Vitae</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
			</div>
		</nav> -->

<!-- *** Presentation *** -->

<div class="container">
	<div class="presentation">

		<h1 class="titre_presentation">Ma présentation</h1>
			<p class="sous_titre">Presentation de mon parcours</p>

<div class="row">
	<div class="col-md-7">
		<img class="img-responsive" src="img/art1.jpg" alt="">
	</div>
        <div class="col-md-5">
                <h2>Titre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        </div>
    </div>
<div class="row">
	<div class="col-md-7">
		<img class="img-responsive" src="img/art1.jpg" alt="">
	</div>
        <div class="col-md-5">
                <h2>Titre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                
        </div>
    </div>
<div class="row">
	<div class="col-md-7">
		<img class="img-responsive" src="img/art1.jpg" alt="">
	</div>
        <div class="col-md-5">
                <h2>Titre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                
        </div>
    </div>

	</div>
</div>

 <!-- *** Diplomes *** -->

<div class="diplomes">
	<h2 class="titre_diplomes">Presentation des <span>diplômes</span></h2>
<div class="container">
  <div class="ensemble_diplome">
	 <div class="row">
		<div class="col-md-4">
                <h2 class="nom_diplome">Titre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor.</p>
            </div>
		<div class="col-md-4">
                <h2 class="nom_diplome">Titre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor.</p>
            </div>
		<div class="col-md-4">
                <h2 class="nom_diplome">Titre</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor.</p>
            </div>
    	</div>
	 </div>
  </div>
</div>

 <!-- *** Loisirs *** -->

<div class="container">
	<div class="loisirs_bloc">

		<h2 class="titre_loisirs">Mes <span>loisirs</span></h2>
<div class="ensemble_loisirs">
	<div class="row text-center">
            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="img/lois1.jpg" alt="">
                    <div class="caption">
                        <h3>Titre</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="img/lois1.jpg" alt="">
                    <div class="caption">
                        <h3>Titre</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="img/lois1.jpg" alt="">
                    <div class="caption">
                        <h3>Titre</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="img/lois1.jpg" alt="">
                    <div class="caption">
                        <h3>Titre</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                        </p>
                    	</div>
                	</div>
            	</div>
			</div>
		</div>
	</div>
</div>

 <!-- 	*** Footer *** -->
<div class="footer">
	<div class="bloc_logo">
		<a href="https://www.linkedin.com/profile/preview?locale=fr_FR&trk=prof-0-sb-preview-primary-button" target="blank"><img class="img-circle" src="img/logoin.jpg"></a>		
	</div>
</div>


</body>
</html>