<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model();
	}
	public function index()
	{
		$this->load->view('header');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('test');
		$this->load->view('footer');
	}
}
