$(document).ready(function() {

	$(".chat-section > button").click(function() {
		var action = $(this).data("action");
		toggleBodyClasses(action);
	});
});

var toggleBodyClasses = function(action) {
	var body = $("body");

	var flip = ["chat", "list"].indexOf(action) != -1;

	var newClasses = function() {
		if (flip) {
			body.addClass('flip ' + action);
		} else {
			body.addClass(action);
		}
	};

	if (body.hasClass('flip')) {
		body.removeClass().addClass('flip');
		setTimeout(function() {body.removeClass('flip');}, 299);
	} else {
		body.removeClass();
	}
	setTimeout(newClasses, 300);
};