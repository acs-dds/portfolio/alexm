<link rel="stylesheet" type="text/css" href="http://alexm.dijon.codeur.online/achievo/css/theme.css">
<style class="cp-pen-styles">#particles-js{ position:absolute; width: 100%; height: 100%;margin-left:-7%;}</style>
<script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Arimo|Rubik+Mono+One" rel="stylesheet">

<div id="particles-js"></div>
<script>particlesJS("particles-js", {"particles":{"number":{"value":161,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":1,"opacity_min":0,"sync":false}},"size":{"value":2,"random":true,"anim":{"enable":false,"speed":4,"size_min":0.3,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1,"direction":"none","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"bubble"},"onclick":{"enable":true,"mode":"push"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":250,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});var count_particles, stats, update; stats = new Stats; stats.setMode(0); stats.domElement.style.position = 'absolute'; stats.domElement.style.left = '0px'; stats.domElement.style.top = '0px'; document.body.appendChild(stats.domElement); count_particles = document.querySelector('.js-count-particles'); update = function() { stats.begin(); stats.end(); if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) { count_particles.innerText = window.pJSDom[0].pJS.particles.array.length; } requestAnimationFrame(update); }; requestAnimationFrame(update);;
</script>

<div class="col-xs-12 achievo-list">

	<?php foreach($succes as $s): ?>
	<article class="trophy-content">

		<h2 style="font-family: 'Rubik Mono One', sans-serif;font-size:20px;"><?= $s->titre.'<br />'; ?></h2>
		<p style="font-family: 'Arimo', sans-serif;"><?= $s->intitule.'<br />'; ?></p>

		<div class="block">
        <div class="progress">
            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar"
						     aria-valuenow="75" aria-valuemin="0" aria-valuemax="<?= $s->objectif; ?>" style="width: <?= $s->progression; ?>%;">
            </div>
        </div>
		</div>

		<a href="<?php echo site_url('achievo/view/'.$s->id); ?>"	><button class="btn-maj">Mettre à jour</button></a>

	</article>
	<?php endforeach; ?>
</div>
