<!DOCTYPE html>
<html>
<head>

	<title>ACS | Alex</title>
	<link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="static/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Faster+One|Telex" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="static/css/font-awesome.min.css">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                  <p class="logo">OnlineFormaPro</p>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.html">Accueil</a>
                    </li>
                    <li>
                        <a href="#">Projets</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

<video autoplay="" loop="" poster="" id="bgvid">
    <source id="vid" src="http://alexm.dijon.codeur.online/static/img/fond.mp4" type="video/mp4">
</video>

    <h2 class="titre">Bienvenue sur la page des projets !</h2>
<div class="pro">
    <ul class="projets">
        <li>Slider d'Images</li><a href="projets/Carroussel/index.html"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>Génerateur de Lorem</li><a href="projets/Lorem/index.php"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>Projet NODEX</li><a href="projets/NODEX/Index.php"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>Portfolio</li><a href="projets/Portfolio/index.php"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>TCHAT</li><a href="projets/TCHAT/"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>Deezer</li><a href="projets/Deezer/index.html"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>SimonGame</li><a href="projets/Simon/simon.html"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>SVGBounce</li><a href="projets/SvgBounce/raph.html"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
        <li>SVGCircle</li><a href="projets/SvgCircle/index.html"><button id="btn" type="button">▁▂▃▅▆▇ Clique ▇▆▅▃▂▁</button></a>
    </ul>
</div>

<div class="footer">

<p class="copy">&copy;Alex | ACS-Projets OFP<br>2016-2017 | Dijon</p>
<p class="copy2">Version 1.1.1</p>
<div class="icones">
	<i class="fa fa-linkedin" aria-hidden="true"></i>
</div>
</div>

</body>
</html>